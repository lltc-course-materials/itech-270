---
layout: default
---

# ITECH 270 Course Materials

* <a href="source/source.zip">Source Code</a>

<hr>

To look at the source code of any example in most browsers, press **ctrl-u**.

## Chapter 1 - Introduction to Web Programming

* [Slides](slides/chap1.pptm)
* [Example 1](source/chap1/weather.html)
* [Case Study](source/sourceCodeCaseStudies/chapter01/microgrid/pages/electricPowerHistory.html)

<hr>

## Chapter 2 - Coding Standards, Block Elements, Text Elements, and Character References

* [Slides](slides/chap2.pptm)
* Examples
    * [Roosevelt 1](source/chap2/rooseveltQuote1.html)
    * [Haiku](source/chap2/haiku.html)
    * [Roosevelt 2](source/chap2/rooseveltQuote2.html)
    * [w3cBarbeque](source/chap2/w3cBarbeque.html)
    * [Coding Elements](source/chap2/codingElements.html)
    * [Pizzeria](source/chap2/pizzeria.html)
* Case Study
    * [Electric Power History](source/sourceCodeCaseStudies/chapter02/microgrid/pages/electricPowerHistory.html)
    * [Lawrence Hydropower](source/sourceCodeCaseStudies/chapter02/microgrid/pages/lawrenceHydropower.html)

## Chapter 3 - Cascading Style Sheets (CSS)

* [Slides](slides/chap3.pptm)
* Examples
    * [Poem](source/chap3/treePoem.html)
    * [Quotes](source/chap3/markTwainQuotes.html)
    * [Halloween](source/chap3/pumpkinPatch.html)
    * [Halloween External](source/chap3/pumpkinPatchExternalCss.html)
    * [Opacity](source/chap3/opacityExample.html)
    * [Box](source/chap3/boxExample.html)
    * [Hot or Cold](source/chap3/hotCold.html)
    * [Declaration of Independence](source/chap3/declarationOfIndependence.html)
* Case Study
    * [Electric Power History](source/sourceCodeCaseStudies/chapter03/microgrid/pages/electricPowerHistory.html)
    * [Lawrence Hydropower](source/sourceCodeCaseStudies/chapter03/microgrid/pages/lawrenceHydropower.html)
    * [Area Description](source/sourceCodeCaseStudies/chapter03/microgrid/pages/areaDescription.html)

# Chapter 4 - Lists

* [Slides](slides/chap4.pptm)

# Chapter 5 - Tables

* [Slides](slides/chap5.pptm)

# Chapter 6 - Links and Images

* [Slides](slides/chap6.pptm)

* Case Study
    * [Index](source/sourceCodeCaseStudies/chapter06/microgrid/pages/index.html)
